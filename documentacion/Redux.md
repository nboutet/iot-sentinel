# Flujo de redux


##Configuración de redux:
  Creamos una aplicación que estará contenida en el wrapper "Provider". Provider posee una 
  prop "store" que recibe un store (antes creado) que contiene los reducers que se encargan
  de la manipulación de los estados de la aplicación.
  Dentro de la app es necesario conectar a dicho componente con el estado de la app
  Luego se pasa la app creada al DOM de React para que se renderice.

## Explicación / ejemplo del flujo de actions y reducers
Tomemos como ejemplo actions y reducers de la autenticación.

Caso Login:
    Cuando el usuario intenta loguearse, con un username y password, se ejecuta el método "authLogin" (dentro del archivo actions/auths). A continuación se ejecuta el dispatch del action "authStart" y dependiendo de la respuesta de la API, tras la llamada mediante Axios,se ejecuta el dispatch de actions específicas: authSuccess o authFail. 
    Estos actions retornan un objeto que contiene un campo con tipo de action ejecutado y algún campo adicional. 
    Cuando se ejecutan éstos actions, tambien se invoca al reducer correspondiente, el cual es el encargado de modificar(o actualizar) el estado de la aplicación completa.

