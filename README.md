# IOT
Sistema de monitorización web para gestionar datos hidroambientales.

Este proyecto está divido en backend y frontend. El backend contiene un proyecto Django el cual utiliza Django Rest Framework para alojar una API. El frontend utiliza ReactJS y consulta datos de la API.


## Tecnologías

- Backend:
    - Django / Django Rest Framework
    - DB: mysql / mariadb?
- Frontend 
    - React
    - Redux (Manejador de estados de la app) 
    - Antd (Diseño de las vistas - https://ant.design/ ) 
- Despliegue temporal
    - Heroku
- Despliegue final
    - Debian GNU/Linux stable (apache)
    - docker? 
    - ¿otra?


# Guía de instalación del proyecto
1.  Clonar proyecto

## Backend
    -Version de Python: 3.6.5

2.  Abrir una consola, posicionarse en la carpeta del proyecto y ejecutar los siguientes comandos:

`cd iot/backend/`

3.  En caso de no tener la version de Python 3.6.5, instalarla.

4.  Ejecutar:

`virtualenv env`

`source env/bin/activate`

5.  Chequear que la version de python instalada en el virtualenv sea la 3.6.5
6.  Ejecutar:  

`pip install -r requirements.txt`

`cd src/`

`python manage.py runserver`

## Frontend
Npm version: 6.4.1
7.  Abrir otra consola, posicionarse en la carpeta del proyecto y ejecutar los siguientes comandos:

`cd iot/frontend/gui/`

`npm i`

`npm run build`

`npm start`

 
 
## Referencias

- https://stackoverflow.com/questions/28610372/reactjs-with-django-real-usage#28646223
- https://hackernoon.com/creating-websites-using-react-and-django-rest-framework-b14c066087c7
- https://docs.google.com/presentation/d/1TRgDA-fImXTj88eeNW37iokt4ehuK8Q6Y-7hZnBqW4g/edit#slide=id.g1559307c6d_0_371
- https://argentinaenpython.com/django-girls/tutorial/
- https://help.pythonanywhere.com/pages/UsingMySQL


## 