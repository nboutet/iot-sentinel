import React from 'react';
import { Route, Switch } from 'react-router-dom';

import DispositivosList from './containers/DispositivosListView';
import DispositivoDetail from './containers/DispositivoDetailView';
import VariableDetail from './containers/VariableDetailView';
import Login from './containers/Login';
import Signup from './containers/Signup';


export const ServerDomain = 'https://iot-sentinel.herokuapp.com';
//export const ServerDomain = 'http://127.0.0.1:8000';



const BaseRouter = () => (
    <div>
        <Switch>
            <Route exact path='/' component={DispositivosList} /> {" "}
            <Route exact path='/dispositivos/:dispositivoID/' component={DispositivoDetail} /> {" "}
            <Route exact path='/dispositivos/:dispositivoID/variables/:variableID/' component={VariableDetail} /> {" "}
            <Route exact path='/login/' component={Login} /> {" "}
            <Route exact path='/signup/' component={Signup} /> {" "}
        </Switch>
    </div>
)

export default BaseRouter;