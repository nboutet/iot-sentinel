// En este  archivo se define un metodo por cada tipo de action declarado en './actionTypes.js'
import * as actionTypes from './actionTypes';
import axios from 'axios';
import store from '../configureStore';

import { ServerDomain } from '../../routes';

// FETCH LIST OF VARIABLES
export const fetchDatosStarted = () => ({
    type: actionTypes.FETCH_DATOS_STARTED
});

export const fetchDatosSucceeded = (datos) => ({
    type: actionTypes.FETCH_DATOS_SUCCEEDED,
    payload: [...datos]
});

export const fetchDatosFailed = (error) => ({
    type: actionTypes.FETCH_DATOS_FAILED,
    error: error
});



export const fetchDatos = (token, variableID) => {
    return dispatch => {
        // const token = store.getState().authentication.token;

        if (token) {

            axios.defaults.headers = {
                "Content-Type": "application/json",
                "Authorization": `Token ${token}`,
            }
            dispatch(fetchDatosStarted());
            axios.get(`${ServerDomain}/api/datos/`)
                .then(response => {
                    dispatch(fetchDatosSucceeded(response.data.results));
                })
                .catch(error => {
                    dispatch(fetchDatosFailed(error.message))
                    console.log("error VariableRealTimeChart")
                    // dispatch(fetchVariableFailed(error.message));
                });
        }
    }
}
