// Listado de nombres de tipos de Actions que pueden ejecutarse

//  -------- >  LOGIN  <------
export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const RESET = 'RESET';


// ----------> DISPOSITIVOS  <-------------
export const FETCH_DISPOSITIVOS_STARTED = 'FETCH_DISPOSITIVOS_STARTED';
export const FETCH_DISPOSITIVOS_SUCCEEDED = 'FETCH_DISPOSITIVOS_SUCCEEDED';
export const FETCH_DISPOSITIVOS_FAILED = 'FETCH_DISPOSITIVOS_FAILED';

export const FETCH_DISPOSITIVO_STARTED = 'FETCH_DISPOSITIVO_STARTED';
export const FETCH_DISPOSITIVO_SUCCEEDED = 'FETCH_DISPOSITIVO_SUCCEEDED';
export const FETCH_DISPOSITIVO_FAILED = 'FETCH_DISPOSITIVO_FAILED';

export const CREATE_DISPOSITIVO_STARTED = 'CREATE_DISPOSITIVO_STARTED';
export const CREATE_DISPOSITIVO_SUCCEEDED = 'CREATE_DISPOSITIVO_SUCCEEDED';
export const CREATE_DISPOSITIVO_FAILED = 'CREATE_DISPOSITIVO_FAILED';

export const UPDATE_DISPOSITIVO_STARTED = 'UPDATE_DISPOSITIVO_STARTED';
export const UPDATE_DISPOSITIVO_SUCCEEDED = 'UPDATE_DISPOSITIVO_SUCCEEDED';
export const UPDATE_DISPOSITIVO_FAILED = 'UPDATE_DISPOSITIVO_FAILED';

export const DELETE_DISPOSITIVO_STARTED = 'DELETE_DISPOSITIVO_STARTED';
export const DELETE_DISPOSITIVO_SUCCEEDED = 'DELETE_DISPOSITIVO_SUCCEEDED';
export const DELETE_DISPOSITIVO_FAILED = 'DELETE_DISPOSITIVO_FAILED';


// ------------> VARIABLES  <-------------
export const FETCH_VARIABLES_STARTED = 'FETCH_VARIABLES_STARTED';
export const FETCH_VARIABLES_SUCCEEDED = 'FETCH_VARIABLES_SUCCEEDED';
export const FETCH_VARIABLES_FAILED = 'FETCH_VARIABLES_FAILED';

export const FETCH_VARIABLE_STARTED = 'FETCH_VARIABLE_STARTED';
export const FETCH_VARIABLE_SUCCEEDED = 'FETCH_VARIABLE_SUCCEEDED';
export const FETCH_VARIABLE_FAILED = 'FETCH_VARIABLE_FAILED';

export const CREATE_VARIABLE_STARTED = 'CREATE_VARIABLE_STARTED';
export const CREATE_VARIABLE_SUCCEEDED = 'CREATE_VARIABLE_SUCCEEDED';
export const CREATE_VARIABLE_FAILED = 'CREATE_VARIABLE_FAILED';

export const UPDATE_VARIABLE_STARTED = 'UPDATE_VARIABLE_STARTED';
export const UPDATE_VARIABLE_SUCCEEDED = 'UPDATE_VARIABLE_SUCCEEDED';
export const UPDATE_VARIABLE_FAILED = 'UPDATE_DISPOSITIVO_FAILED';

export const DELETE_VARIABLE_STARTED = 'DELETE_VARIABLE_STARTED';
export const DELETE_VARIABLE_SUCCEEDED = 'DELETE_VARIABLE_SUCCEEDED';
export const DELETE_VARIABLE_FAILED = 'DELETE_VARIABLE_FAILED';



//---------------> DATOS <-----------------------

export const FETCH_DATOS_STARTED = 'FETCH_DATOS_STARTED';
export const FETCH_DATOS_SUCCEEDED = 'FETCH_DATOS_SUCCEEDED';
export const FETCH_DATOS_FAILED = 'FETCH_DATOS_FAILED';
