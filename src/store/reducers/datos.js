import * as actionTypes from '../actions/actionTypes';
import { updateList, arrayToObject } from '../utility';




const initialState = {
    data: [],     // Lista de variables
    loading: false,
    error: false,
}

export const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.FETCH_DATOS_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_DATOS_SUCCEEDED:

            const newData = action.payload.map((item, index) => {
                return {
                    date: new Date(Date.parse(item.fecha)),
                    Variable: item.valor
                };
            });


            state = {
                ...state,
                //data: arrayToObject([...action.payload]),
                data: [...newData],
                loading: false
            };
            break;

        case actionTypes.FETCH_DATOS_FAILED:
            state = {
                ...state,
                data: null,
                error: action.error,
                loading: false
            };
            break;

        default:
            return state;
    }

    return state;
}                