import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router'

import * as serviceWorker from './serviceWorker';

import store, { persistor, history } from './store/configureStore';
import { PersistGate } from 'redux-persist/integration/react'

import './css/styles.css'
import App from './App';

// const initialState = {};
//const store = configureStore(initialState)


//Configuración de redux:
//  Creamos una aplicación que estará contenida en el wrapper "Provider". Provider posee una 
//  prop "store" que recibe un store (antes creado) que contiene los reducers que se encargan
//  de la manipulación de los estados de la aplicación.
const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <PersistGate loading={null} persistor={persistor}>
                <App />
            </PersistGate>
        </ConnectedRouter>
    </Provider >
)

//Luego se pasa la app creada al DOM de React para que se renderice.
ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.register();
serviceWorker.unregister();
