import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { push } from 'connected-react-router'

import { Drawer, Form, Button, Col, Row, Input, Icon, Select } from 'antd';

import * as actions from '../store/actions/dispositivos';

const { Option } = Select;

class DispositivoFormView extends React.Component {

  constructor(props) {
    super(props);


    this.state = {
      estado: 'True',
      visible: false,
    };

    this.handleEstadoChange = this.handleEstadoChange.bind(this);
  }


  //Despliega el Drawer del formulario
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  //Cierra el Drawer del formulario
  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  handleEstadoChange(estadoValue) {
    this.setState({ estado: estadoValue });
  }

  handleSubmit = (event) => {

    const dispositivo = this.props.dispositivo;

    //Prepara el objeto Dispositivo con los datos extraidos del formulario
    const postObj = {
      etiqueta: event.target.elements.etiqueta.value,
      descripcion: event.target.elements.descripcion.value,
      estado: this.state.estado,
      latitud: event.target.elements.latitud.value,
      longitud: event.target.elements.longitud.value,
    }

    this.props.onSubmitDispositivo(event, postObj, this.props.requestType, dispositivo);
    this.onClose();
  }


  render() {

    const { size } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer} style={{ float: 'right' }} >
          <Icon type="plus" /> {this.props.btnText}
        </Button>

        <Drawer
          title="Dispositivo"
          width={720}
          onClose={this.onClose}
          visible={this.state.visible}
        >

          <Form layout="vertical" hideRequiredMark
            onSubmit={(event) => this.handleSubmit(event)}>

            <Row gutter={16}>

              <Col span={12}>
                <Form.Item label="Etiqueta">
                  {getFieldDecorator('etiqueta', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.etiqueta : "",
                    rules: [{ required: true, message: "Por favor un identificador del dispositivo" }],
                  })(<Input name="etiqueta" placeholder="Ingrese identificador del dispositivo" />)}
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label="Estado">
                  {getFieldDecorator('estado', {
                    initialValue: this.state.estado,
                    rules: [{ required: true, message: 'Por favor elija un estado!' }],
                  })(
                    <Select
                      name="estado"
                      onChange={this.handleEstadoChange}
                      placeholder="Elija un estado"
                      size={size}
                      style={{ width: '60%' }}
                    >
                      <Option value="True">Habilitado</Option>
                      <Option value="False">No Habilitado</Option>
                    </Select>,
                  )}
                </Form.Item>
              </Col>

            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Latitud">
                  {getFieldDecorator('latitud', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.latitud : "",
                    rules: [{ required: false, message: "Por favor ngrese longitud de la ubicación del dispositivo" }],
                  })(<Input name="latitud" placeholder="Ingrese longitud de la ubicación del dispositivo" />)}
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label="Longitud">
                  {getFieldDecorator('longitud', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.longitud : "",
                    rules: [{ required: false, message: "Por favor ingrese longitud de la ubicación del dispositivo" }],
                  })(
                    <Input name="longitud" style={{ width: '100%' }} placeholder="Ingrese longitud de la ubicación del dispositivo" />,
                  )}
                </Form.Item>
              </Col>

            </Row>


            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Descripcion">
                  {getFieldDecorator('descripcion', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.descripcion : "",
                    rules: [
                      {
                        required: false,
                        message: 'Por favor ingrese descripción',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="Ingrese descripción" />)}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: 'absolute',
                left: 0,
                bottom: 0,
                width: '100%',
                borderTop: '1px solid #e9e9e9',
                padding: '10px 16px',
                background: '#fff',
                textAlign: 'right',
              }}
            >

              <Button onClick={this.onClose} style={{ marginRight: 8 }}> Cancelar </Button>
              <Button type="primary" htmlType="submit"> Guardar </Button>

            </div>
          </Form>
        </Drawer>

      </div >
    );
  }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
export const mapStateToProps = state => {
  return {
    token: state.authentication.token,
    // pathname: state.router.location.pathname
  }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
  return {
    // handleRedirect: (url) => dispatch(push(url)),
    onSubmitDispositivo: (event, postObj, requestType, dispositivo) => dispatch(actions.handleSubmit(event, postObj, requestType, dispositivo)),
  }
}

const DispositivoForm = Form.create()(DispositivoFormView);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DispositivoForm));