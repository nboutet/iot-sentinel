import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router'
import { Button, Table, Divider, Tag } from 'antd';

import * as actions from '../store/actions/variables';

class VariablesTable extends React.Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     dispositivoID: this.props.dispositivoID,
        // }
    }


    columns = [
        {
            title: 'N°',
            dataIndex: 'id',
            key: 'id'
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre'
        },
        {
            title: 'Etiqueta',
            dataIndex: 'etiqueta',
            key: 'etiqueta'
        },
        {
            title: 'Unidad',
            dataIndex: 'unidad',
            key: 'unidad',
        },
        {
            title: 'Valor Mín. Permitido',
            key: 'valor_min_permitido',
            dataIndex: 'valor_min_permitido',
        },
        {
            title: 'Valor Máx. Permitido',
            key: 'valor_max_permitido',
            dataIndex: 'valor_max_permitido',
        },
        {
            title: 'Fecha Creación',
            dataIndex: 'fecha_creacion',
            key: 'fecha_creacion',
        },
        {
            title: 'Acción    ',
            key: 'action',
            columnWidth: '20%',
            render: (text, item) => (
                <div>
                    <Button type="primary" onClick={() => this.props.onRedirect(`/dispositivos/${this.props.dispositivoID}/variables/${item.id}/`)} >Ver</Button>
                    <Divider type="vertical" />
                    <Button type="danger" onClick={() => this.props.onDelete(this.props.dispositivoID, item.id)}> Eliminar </Button>
                </div>
            ),
        },
    ];

    componentWillReceiveProps(newProps) {
        if (newProps.token && newProps.token !== this.props.token) {
            this.props.onFetchVariables();
        }
    }

    render() {
        return (
            <div>
                <Table rowKey="id" columns={this.columns} dataSource={this.props.variables} />
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        variables: state.entities.variables.data,
        // dispositivos: selectors.objectToArray(state.entities.dispositivos.data),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDelete: (token, dispositivoID) => dispatch(actions.handleDeleteVariable(token, dispositivoID)),
        onRedirect: (url) => dispatch(push(url)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VariablesTable);

