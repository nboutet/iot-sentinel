import React, { PureComponent } from 'react';
import axios from 'axios';

import RTChart from 'react-rt-chart';
import '../../../node_modules/c3/c3.min.css';


import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Permissions from "react-redux-permissions"



// import * as actions from '../../store/actions/datos';
import * as actions from '../../store/actions/datos';
import { Spin } from 'antd';

import { ServerDomain } from '../../routes';


export default class VariableRealTimeChart extends React.PureComponent {

    constructor(props) {
        super(props);
    }

    state = {
        data: [],
        initialData: null,
    };


    /**
     * Cada 5 segundos se dispara una consulta al servidor, que devuelve un array con
     * los ultimos ¿50? datos registrados paginados de la variable específicada. 
     * 
     * En el primer disparo:
     *          - Se setea el initialState con los primeros 49 registros de esa ultima página 
     *            y se pasan al componente que grafica.
     * 
     * En cada disparo siguiente:
     *          - Se extrae el último elemento del array y se setea en el state del componente
     *            state.data = Ultimo nuevo dato
     *          - Una vez seteado el nuevo dato, se pasa como una prop al componente <RTChart />
     *            que se encarga de agregar dicho dato en la gráfica.
     */

    async componentDidMount() {

        try {
            this.myInterval = setInterval(async () => {
                const token = this.props.token;
                const variableID = this.props.variableID;

                if (token && variableID) {
                    axios.defaults.headers = { "Content-Type": "application/json", "Authorization": `Token ${token}` }

                    await axios.get(`${ServerDomain}/api/datos/?page=last&variable=${variableID}`)
                        .then(response => {

                            const lastItem = response.data.results.pop();

                            const newLastData = {
                                date: new Date(Date.parse(lastItem.fecha)),
                                Variable: lastItem.valor
                            }

                            if (JSON.stringify(this.state.data) !== JSON.stringify(newLastData)) {
                                this.setState({ data: newLastData });
                            }

                            if (!this.state.initialData) {
                                const initialData = [...response.data.results.map((item, index) => {
                                    return {
                                        date: new Date(Date.parse(item.fecha)),
                                        Variable: item.valor
                                    };
                                })]

                                this.setState({ initialData: initialData })
                            }
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            }
                , 5000);

        } catch (error) {
            console.log("error VariableRealTimeChart", error);
        }
    }

    /** 
     * Unmount the component and clear the interval to stop executing the fetching periodically when
     * changing route in the browser
    */
    componentWillUnmount() {
        clearInterval(this.myInterval);
    }


    render() {

        var chart = {
            // axis: {
            //     y: { min: 50, max: 100 }
            // },
            // point: {
            //     show: false
            // }
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        count: 20
                    }
                }
            }
        };

        return (
            <div>

                {!this.state.initialData ?

                    <div className="spin-loading">
                        <Spin tip="Obteniendo datos.." />
                    </div>
                    :
                    <RTChart
                        fields={['Variable']}
                        initialData={this.state.initialData}
                        data={this.state.data}
                        chart={chart}
                    />
                }
            </div>
        );
    }
}



// const mapStateToProps = state => {
//     return {
//         token: state.authentication.token,
//         // datos: selectors.objectToArray(state.entities.datos.data),
//         datos: state.entities.datos.data,
//         // dispositivos: selectors.objectToArray(state.entities.dispositivos.data),
//     }
// }

// const mapDispatchToProps = dispatch => {
//     return {
//         onFetchDatos: (token, variableID = null) => dispatch(actions.fetchDatos(token, variableID)),
//     }
// }


// export default connect(mapStateToProps, mapDispatchToProps)(VariableRealTimeChart);
