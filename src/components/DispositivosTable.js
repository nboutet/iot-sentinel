import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router'
import { Button, Table, Divider, Tag } from 'antd';

import * as actions from '../store/actions/dispositivos';
import * as selectors from '../store/selectors/dispositivos';

class DispositivosTable extends React.Component {




  columns = [
    {
      title: 'N°',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Estacion',
      dataIndex: 'etiqueta',
      key: 'etiqueta'
    },
    {
      title: 'Descripcion',
      dataIndex: 'descripcion',
      key: 'descripcion',
    },
    {
      title: 'Fecha Creación',
      dataIndex: 'fecha_creacion',
      key: 'fecha_creacion',
    },
    {
      title: 'Estado',
      key: 'estado',
      dataIndex: 'estado',
      render: (estado) => {
        let color = estado === true ? 'green' : 'volcano';
        let texto = estado === true ? 'Habilitado' : 'No Habilitado';
        return (
          <Tag color={color} key={estado}>
            {texto}
          </Tag>
        );
      },
    },
    {
      title: 'Ultima Actividad',
      dataIndex: 'ultima_actividad',
      key: 'ultima_actividad',
    },
    {
      title: 'Acción    ',
      key: 'action',
      columnWidth: '20%',
      render: (text, item) => (
        <div>
          <Button type="primary" onClick={() => this.props.onRedirect(`/dispositivos/${item.id}/`)} >Ver</Button>
          <Divider type="vertical" />
          <Button type="danger" onClick={() => this.props.onDeleteDispositivo(item.id)} >Eliminar</Button>
        </div>
      ),
    },
  ];


  componentWillReceiveProps(newProps) {
    if (newProps.token && newProps.token !== this.props.token) {
      this.props.onFetchDispositivos();
    }
  }

  render() {
    return (
      < Table rowKey="id" columns={this.columns} dataSource={this.props.dispositivos} />
    )
  }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivosList'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {
  return {
    token: state.authentication.token,
    dispositivos: state.entities.dispositivos.data,
    // dispositivos: selectors.objectToArray(state.entities.dispositivos.data),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    //onFetchDispositivos: () => dispatch(actions.fetchDispositivos()),
    onDeleteDispositivo: (dispositivoID) => dispatch(actions.handleDeleteDispositivo(dispositivoID)),
    onRedirect: (url) => dispatch(push(url)),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(DispositivosTable);

