// import React from 'react';
// import { connect } from 'react-redux';
// import { withRouter } from 'react-router-dom';
// import { push } from 'connected-react-router'
// import Permissions from "react-redux-permissions"
// import { Descriptions, Badge, Divider, Spin } from 'antd';

// var LineChart = require('react-d3/linechart').LineChart;

// // import { LineChart } from 'react-d3';



// export default class VariableChart extends React.Component {
//     constructor(props) {
//         super(props);

//     }


//     lineData = [
//         {
//             name: "series1",
//             values: [{ x: 0, y: 20 }, { x: 24, y: 10 }, { x: 30, y: 10 }, { x: 45, y: 10 }, { x: 50, y: 10 }],
//             strokeWidth: 3,
//             strokeDashArray: "5,5",
//         }
//     ];

//     viewBox = { x: 0, y: 0, width: 500, height: 400 }


//     render() {
//         return (
//             <div>
//                 <LineChart
//                     legend={true}
//                     data={this.lineData}
//                     width='100%'
//                     height={400}
//                     viewBoxObject={this.viewBox}
//                     title="Line Chart"
//                     yAxisLabel="Altitude"
//                     xAxisLabel="Elapsed Time (sec)"
//                     gridHorizontal={true}
//                 />
//             </div>
//         );
//     }
// }