import React from 'react';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import { Form, Icon, Input, Button, Spin, Alert } from 'antd';

import * as actions from '../store/actions/auth';



class NormalLoginForm extends React.Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onAuth(values.username, values.password, this.props.history);
            }
        });
    };


    render() {

        let errorMessage = null;
        if (this.props.error) {
            const msg = "Error de inicio sesión"
            errorMessage = <Alert message={msg} description={this.props.error} type="warning" />
        }

        const { getFieldDecorator } = this.props.form;
        return (

            <div>
                {errorMessage}

                {this.props.loading ?
                    <div className="spin-loading">
                        <Spin tip="Cargando..." />
                    </div>

                    // <Alert
                    // message=""
                    // description=""
                    //type="info"
                    // />
                    //     </Spin>

                    :

                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Por favor, ingrese su nombre de usuario!' }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Nombre de usuario"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Por favor ingrese su contraseña!' }],
                            })(
                                <Input
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="Contraseña"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{ marginRight: '10px' }}>
                                Iniciar Sesión
                            </Button>

                            <NavLink style={{ marginRight: '30px' }} to="/signup/">
                                Crear Cuenta
                            </NavLink>

                        </Form.Item>
                    </Form>
                }
            </div>

        );
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);

//Mapea el State de la app para que pueda ser utilizado como una propiedad: loading.
const mapStateToProps = state => {
    return {
        loading: state.authentication.loading,
        error: state.authentication.error
    }
}


// Mapea el método "onAuth" como una propiedad del componente. Siendo onAuth la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password, history) => dispatch(actions.authLogin(username, password, history))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm));