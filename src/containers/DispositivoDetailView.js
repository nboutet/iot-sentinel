import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { push } from 'connected-react-router'
import Permissions from "react-redux-permissions"
import { Descriptions, Badge, Divider, Spin } from 'antd';

import * as actions from '../store/actions/dispositivos';
import * as selectors from '../store/selectors/dispositivos';

import DispositivoForm from '../components/DispositivoForm';
import VariablesList from './VariablesListView';
import VariableForm from '../components/VariableForm';


class DispositivoDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // dispositivo: {}
            dispositivoID: this.props.match.params.dispositivoID,
        }

    }



    //Fetch inicial del dispositivo en el componente
    componentDidMount() {
        const dispositivoID = this.props.match.params.dispositivoID;
        if (dispositivoID) {
            this.props.onFetchDispositivo(dispositivoID);
        }
    }

    //Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
    // componentDidMount no sirve en éste caso porque se ejecuta SOLO cuando se monta el componente
    // y el token aún sería null pues el login y las actions que modifican el estado del token se ejecutan
    // posteriormente.

    // Cuando el Formulario de Edición de dispositivo modifique a dicho dispositivo 
    // es decir a la propiedad del componente: "entities.dispositivo", se renderizará nuevamente la vista con los datos actualziados.
    // componentWillReceiveProps(newProps) {
    //     const dispositivoID = this.props.match.params.dispositivoID;
    //     // const newDispositivoID = newProps.match.params.dispositivoID;
    //     // console.log("dispositivoID", dispositivoID);
    //     // console.log("newDispositivoID", newDispositivoID);

    //     console.log("componentWillReceiveProps", newProps)
    //     // if (newProps.token && newProps.token !== this.props.token && newDispositivoID && newDispositivoID !== dispositivoID) {
    //     if (newProps.token && dispositivoID && newProps.dispositivo !== this.props.dispositivo) {

    //         this.props.onFetchDispositivo(dispositivoID);
    //     }
    // }

    // shouldComponentUpdate(){

    // }

    render() {
        return (

            <div>

                <Permissions allowed={["admin"]}>
                    <DispositivoForm
                        {...this.props}
                        requestType="put"
                        btnText="Modificar Dispositivo"
                    />
                </Permissions>

                <Descriptions title="Dispositivo Info" layout="vertical" bordered>
                    <Descriptions.Item label="Dispositivo" span={1}>{this.props.dispositivo ? this.props.dispositivo.etiqueta : ""}</Descriptions.Item>
                    <Descriptions.Item label="Estado" span={1}>
                        {this.props.dispositivo ?
                            <Badge status="success" text="En línea" /> :
                            <Badge status="error" text="Fuera de línea" />
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label="Ultima Actividad" span={1}>{this.props.dispositivo ? this.props.dispositivo.ultima_actividad : ""}</Descriptions.Item>

                    {this.props.dispositivo ?
                        <Descriptions.Item label="Localización" span={1} >{'(' + this.props.dispositivo.latitud + ' ; ' + this.props.dispositivo.longitud + ')'}</Descriptions.Item>
                        :
                        <Descriptions.Item label="Localización" span={1} >{'-'}</Descriptions.Item>
                    }

                    <Descriptions.Item label="Fecha Creación:" span={2}> {this.props.dispositivo ? this.props.dispositivo.fecha_creacion : ""} </Descriptions.Item>


                    {/* <Descriptions.Item label="Usage Time" span={3}>
                        2019-04-24 18:00:00
                    </Descriptions.Item> */}

                    <Descriptions.Item label="Descripción" span={3}>
                        {this.props.dispositivo ? this.props.dispositivo.descripcion : ""}
                    </Descriptions.Item>

                </Descriptions>

                <Divider />
                {/* <VariableForm
                    {...this.props}
                    token={this.props.token}
                    requestType="post"
                    dispositivo={this.props.dispositivo}
                    btnText="Nueva Variable"
                />  */}

                {/* <VariablesList dispositivoID={this.props.dispositivo.id} /> */}
                <VariablesList {...this.props} dispositivoID={this.state.dispositivoID} />
            </div>
        );
    }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {
    // console.log("DetailStateToProps", state.authentication.token);

    return {
        token: state.authentication.token,
        dispositivo: selectors.selectCurrentDispositivo(state.entities.dispositivos.data, state.entities.dispositivos.currentId),
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        handleRedirect: () => dispatch(push('/')),
        onFetchDispositivo: (dispositivoID) => dispatch(actions.fetchDispositivo(dispositivoID)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DispositivoDetail));