import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Collapse, Divider } from 'antd';

import * as actions from '../store/actions/dispositivos';
import * as selectors from '../store/selectors/dispositivos';

import DispositivosTable from '../components/DispositivosTable';
import DispositivoForm from '../components/DispositivoForm';



class DispositivosList extends React.Component {
    // constructor(props) {
    //     super(props);

    //     // this.state = {
    //     //     dispositivos: []
    //     // }

    //     // this.handleDelete = this.handleDelete.bind(this);
    // }


    //Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
    // componentDidMount no sirve en éste caso porque se ejecuta SOLO cuando se monta el componente
    // y el token aún sería null pues el login y las actions que modifican el estado del token se ejecutan
    // posteriormente.

    componentDidMount() {
        if (this.props.token) {
            this.props.onFetchDispositivos();
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.token && newProps.token !== this.props.token) {
            this.props.onFetchDispositivos();
        }
    }

    render() {
        return (
            <div>
                <Permissions allowed={["admin"]}>
                    <DispositivoForm
                        {...this.props}
                        requestType="post"
                        btnText="Nuevo Dispositivo"
                    />
                </Permissions>

                <Divider />

                <Permissions allowed={["admin", "visitor"]}>
                    <DispositivosTable />
                </Permissions>
            </div>
        )
    }
}


// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivosList'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        dispositivos: selectors.objectToArray(state.entities.dispositivos.data),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchDispositivos: () => dispatch(actions.fetchDispositivos()),
        // onDeleteDispositivo: (dispositivoID) => dispatch(actions.handleDeleteDispositivo(dispositivoID)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DispositivosList);