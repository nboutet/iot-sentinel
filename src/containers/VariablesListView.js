import React from 'react';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Button, Table, Divider, Typography } from 'antd';
import { withRouter } from 'react-router-dom';

import * as actions from '../store/actions/variables';
import * as selectors from '../store/selectors/variables'
import * as utility from '../store/utility'

import VariablesTable from '../components/VariablesTable';
import VariableForm from '../components/VariableForm';

const { Title } = Typography;

class VariablesList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dispositivoID: this.props.dispositivoID,
        }
    }

    // columns = [
    //     {
    //         title: 'N°',
    //         dataIndex: 'id',
    //         key: 'id'
    //     },
    //     {
    //         title: 'Nombre',
    //         dataIndex: 'nombre',
    //         key: 'nombre'
    //     },
    //     {
    //         title: 'Etiqueta',
    //         dataIndex: 'etiqueta',
    //         key: 'etiqueta'
    //     },
    //     {
    //         title: 'Unidad',
    //         dataIndex: 'unidad',
    //         key: 'unidad',
    //     },
    //     {
    //         title: 'Valor Mín. Permitido',
    //         key: 'valor_min_permitido',
    //         dataIndex: 'valor_min_permitido',
    //     },
    //     {
    //         title: 'Valor Máx. Permitido',
    //         key: 'valor_max_permitido',
    //         dataIndex: 'valor_max_permitido',
    //     },
    //     {
    //         title: 'Fecha Creación',
    //         dataIndex: 'fecha_creacion',
    //         key: 'fecha_creacion',
    //     },
    //     {
    //         title: 'Acción    ',
    //         key: 'action',
    //         columnWidth: '20%',
    //         render: (text, item) => (
    //             <div>
    //                 <Button type="primary" href={`/dispositivos/${this.props.dispositivoID}/variables/${item.id}/`} >Ver</Button>
    //                 <Divider type="vertical" />
    //                 {/* <Button type="danger" onClick={() => this.props.onDeleteVariable(this.props.dispositivoID, item.id)}> Eliminar </Button> */}
    //             </div>
    //         ),
    //     },
    // ];

    componentDidMount() {

        if (this.props.token && this.props.dispositivoID) {
            this.props.onFetchVariables(this.props.dispositivoID);
        }
    }

    /**
     * Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
     * Permite mantener actualizado el state del componente tras cada  dispatch de un action que 
     * modifque las props del componente: onDeleteVariable ó onFetchVariables
     */
    componentWillReceiveProps(newProps) {

        // Cuando el componente se monta el ID del dispositivo es null:
        // Cuando se termine de actualizar el state del componente y se tenga el ID del dispositivo 
        // se realiza un fetch de las variables. 
        if (this.props.token && this.props.dispositivoID && newProps.dispositivoID !== this.props.dispositivoID) {
            this.props.onFetchVariables(newProps.dispositivoID);
        }
    }


    render() {
        return (
            <div>
                <Title level={4}>Variables</Title>

                <Permissions allowed={["admin"]}>
                    <VariableForm {...this.props} requestType="post" btnText="Crear Variable" dispositivoID={this.props.dispositivoID} variableID={null} />
                </Permissions>

                <Divider type="horizontal" />

                <Permissions allowed={["admin", "visitor"]}>
                    <VariablesTable dispositivoID={this.props.dispositivoID} />
                </Permissions>
            </div>
        )
    }
}


// Mapea token y variables almacenados en el state del Store de la app para que puedan ser utilizados
// como una propiedades en el componente 'VariablesList'.
// Ej: Ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {

    return {
        token: state.authentication.token,
        // variables: state.entities.variables,
        variables: selectors.objectToArray(state.entities.variables.data),
        // dispositivoID: this.state.dispositivoID,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchVariables: (dispositivoID) => dispatch(actions.fetchVariables(dispositivoID)),
        // onDeleteVariable: (dispositivoID, variableID) => dispatch(actions.handleDeleteVariable(dispositivoID, variableID)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VariablesList));