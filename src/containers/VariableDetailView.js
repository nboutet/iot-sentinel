import React, { PureComponent } from 'react';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Descriptions, Spin } from 'antd';

import * as actions from '../store/actions/variables';
import * as selectors from '../store/selectors/variables';

import VariableForm from '../components/VariableForm';

import VariableChart from '../components/charts/VariableChart';

import VariableRealTimeChart from '../components/charts/VariableRealTimeChart';




class VariableDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dispositivoID: this.props.match.params.dispositivoID,
            variableID: this.props.match.params.variableID
        }
    }

    //Fetch inicial del dispositivo en el componente
    componentDidMount() {
        // const dispositivoID = this.props.match.params.dispositivoID;
        if (this.props.token && this.state.dispositivoID && this.state.variableID) {
            this.props.onFetchVariable(this.state.dispositivoID, this.state.variableID);
        }
    }

    // componentDidMount() {
    //     // console.log("newProps")

    //     // if (newProps.dispositivoID !== this.props.dispositivoID)
    //     // console.log("newProps_VariableDetail", newProps);
    //     // console.log("this.props_VariableDetail", this.props);

    //     if (this.state.dispositivoID && this.state.variableID) {
    //         console.log("componentDidMount Variable", this.state.dispositivoID, this.state.variableID);
    //         this.props.onFetchVariable(this.state.dispositivoID, this.state.variableID);
    //     }
    // }

    // //Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
    // // componentDidMount no sirve en éste caso porque se ejecuta SOLO cuando se monta el componente
    // // y el token aún sería null pues el login y las actions que modifican el estado del token se ejecutan
    // // posteriormente.
    // componentWillReceiveProps(newProps) {
    //     // console.log("newProps")

    //     // // if (newProps.dispositivoID !== this.props.dispositivoID)
    //     // console.log("newProps_VariableDetail", newProps);
    //     // console.log("this.props_VariableDetail", this.props);

    //     if (newProps.token !== this.props.token && this.state.dispositivoID && this.state.variableID) {
    //         this.props.onFetchVariable(this.state.dispositivoID, this.state.variableID);
    //     }
    // }

    // shouldComponentUpdate(nextProps, nextState) {
    //     return this.props.variable && nextProps.variable != this.props.variable;
    // }



    render() {
        return (

            <div>

                {this.props.variableObj ?

                    <div>
                        <VariableForm
                            // {...this.props}
                            requestType="put"
                            dispositivoID={this.state.dispositivoID}
                            variableID={this.state.variableID}
                            variableObj={this.props.variableObj}
                            btnText="Editar"
                        />
                        <Descriptions title="Variable Info" layout="vertical" bordered>
                            <Descriptions.Item label="Nombre" span={1}>{this.props.variableObj.nombre}</Descriptions.Item>
                            <Descriptions.Item label="Etiqueta" span={1}>{this.props.variableObj.etiqueta}</Descriptions.Item>
                            <Descriptions.Item label="Dispositivo" span={1}>{this.props.variableObj.dispositivo}</Descriptions.Item>


                            <Descriptions.Item label="Unidad" span={1}>{this.props.variableObj.unidad}</Descriptions.Item>
                            <Descriptions.Item label="Valor mínimo permitido" span={1} >{this.props.variableObj.valor_min_permitido}</Descriptions.Item>
                            <Descriptions.Item label="Fecha máximo permitido" span={1}> {this.props.variableObj.valor_max_permitido} </Descriptions.Item>


                            <Descriptions.Item label="Descripción" span={3}>
                                {this.props.variableObj.descripcion}
                            </Descriptions.Item>

                        </Descriptions>

                        {/* <LineChart /> */}

                        {/* <VariableChart /> */}

                        <VariableRealTimeChart token={this.props.token} variableID={this.state.variableID} />


                    </div>

                    :

                    <div className="spin-loading">
                        <Spin tip="Cargando..." />
                    </div>

                }
            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        variableObj: selectors.selectCurrentVariable(state.entities.variables.data, state.entities.variables.currentId),
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onFetchVariable: (dispositivoID, variableID) => dispatch(actions.fetchVariable(dispositivoID, variableID)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VariableDetail));


