import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Form, Input, Button, Icon, Alert } from 'antd';

import * as actions from '../store/actions/auth';


class RegistrationForm extends React.Component {
    state = {
        confirmDirty: false,
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.props.onAuth(values.username, values.email, values.password1, values.password2);
            }
            //this.props.history.push("/");
        });
    };

    handleConfirmBlur = e => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password1')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['password2'], { force: true });
        }
        callback();
    };



    render() {
        const { getFieldDecorator } = this.props.form;

        let errorMessage = null;
        if (this.props.error) {
            errorMessage = <Alert message="Error al intentar registrarse" description={this.props.error} type="error" />
        }

        return (
            <div>
                {errorMessage}

                {/* <div>
             { this.props.error ?
                 (this.props.error.map((e,i) => <Alert message= "Error de inicio sesión"  description= {e.error[i]} type="warning" /> ))                  
                :
                 <p></p>                  
             }
            </div> */}


                <Form onSubmit={this.handleSubmit}>
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Por favor, ingrese un nombre de usuario!' }],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Nombre de usuario"
                            />,
                        )}
                    </Form.Item>

                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [
                                {
                                    type: 'email',
                                    message: 'El E-mail ingresado no es válido!',
                                },
                                {
                                    required: true,
                                    message: 'Por favor ingrese su E-mail!',
                                },
                            ],
                        })(<Input
                            prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="E-mail" />)
                        }
                    </Form.Item>

                    <Form.Item hasFeedback>
                        {getFieldDecorator('password1', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Por favor ingrese su contraseña!',
                                },
                                {
                                    validator: this.validateToNextPassword,
                                },
                            ],
                        })(<Input.Password
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                        />)
                        }
                    </Form.Item>

                    <Form.Item hasFeedback>
                        {getFieldDecorator('password2', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Por favor confirme su contraseña!',
                                },
                                {
                                    validator: this.compareToFirstPassword,
                                },
                            ],
                        })(<Input.Password
                            onBlur={this.handleConfirmBlur}
                            prefix={<Icon type="lock"
                                style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password" />)}
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" style={{ marginRight: '10px' }}>
                            Registrarse
                    </Button>

                        <NavLink style={{ marginRight: '30px' }} to="/login/">
                            Iniciar sesión
                    </NavLink>
                    </Form.Item>

                </Form>
            </div>

        );
    }
}

const WrappedRegistrationForm = Form.create({ name: 'register' })(RegistrationForm);

//Mapea el State de la app para que pueda ser utilizado como una propiedad: loading.
const mapStateToProps = state => {
    return {
        loading: state.loading.toString(),
        error: state.error
    }
}


// Mapea el método "onAuth" como una propiedad del componente. Siendo onAuth la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, email, password1, password2) => dispatch(actions.authSignup(username, email, password1, password2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedRegistrationForm)