import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import Permissions from "react-redux-permissions"

import { Layout, Menu, Breadcrumb } from 'antd';

import ErrorBoundary from '../components/ErrorBoundary';
import * as actions from '../store/actions/auth';

const { Header, Content, Footer } = Layout;



class CustomLayout extends React.Component {

    render() {
        return (
            <div>
                {/* <ErrorBoundary> */}
                <Layout className="layout">
                    <Header>
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['2']}
                            style={{ lineHeight: '64px' }}
                        >

                            <Menu.Item key="1">
                                <Link to="/">Inicio</Link>
                            </Menu.Item>

                            {
                                this.props.isAuthenticated ?

                                    <Menu.Item key="2" onClick={this.props.logout} style={{ float: 'right' }}>
                                        Cerrar Sesión
                                         </Menu.Item>

                                    :

                                    <Menu.Item key="2" style={{ float: 'right' }}>
                                        <Link to="/login/">Iniciar sesión</Link>
                                    </Menu.Item>

                            }

                        </Menu>
                    </Header>
                    <div>
                        <Content style={{ padding: '0 50px' }}>
                            <Permissions allowed={["admin", "visitor"]}>
                                <Breadcrumb style={{ margin: '16px 0' }}>
                                    <Breadcrumb.Item><Link to="/">Home</Link></Breadcrumb.Item>
                                    <Breadcrumb.Item><Link to="/">List</Link></Breadcrumb.Item>
                                </Breadcrumb>
                            </Permissions>
                            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                                {this.props.children}
                            </div>
                        </Content>
                    </div>


                    <Footer style={{ textAlign: 'center' }}> Sentinel - Sistema de Monitorización de Dispositivos de Medición Automáticos </Footer>
                </Layout>
                {/* </ErrorBoundary> */}
            </div>
        );
    }
}



// Mapea el método "onAuth" como una propiedad del componente. Siendo onAuth la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default withRouter(connect(null, mapDispatchToProps)(CustomLayout));