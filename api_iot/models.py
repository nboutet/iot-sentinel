from django.db import models
import datetime
from django.utils.timezone import now
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext as _
from rest_framework import serializers

ESTADO_CHOICES = (
    (True, 'Habilitado'),
    (False, 'No Habilitado')
)


class Dispositivo(models.Model):

    etiqueta = models.CharField(
        max_length=50, unique=True, blank=False)
    descripcion = models.TextField(max_length=200)
    estado = models.BooleanField(
        choices=ESTADO_CHOICES, default=True, blank=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    ultima_actividad = models.DateTimeField(null=True, blank=True)
    latitud = models.FloatField(_('Latitude'), blank=True, null=True)
    longitud = models.FloatField(_('Longitude'), blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.etiqueta)
        # return "%s" % (self.etiqueta)


# class Localizacion(models.Model):
#     lat = models.FloatField(_('Latitude'), blank=True, null=True)
#     lon = models.FloatField(_('Longitude'), blank=True, null=True)
#     descripcion = models.TextField(max_length=200)
#     fecha_desde = models.DateTimeField(blank=True, null=True)
#     fecha_hasta = models.DateTimeField(blank=True, null=True)
#     dispositivo = models.ForeignKey(Dispositivo, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.descripcion


class Propiedad(models.Model):
    nombre = models.CharField(max_length=25)
    descripcion = models.TextField(max_length=200)
    dispositivo = models.ForeignKey(Dispositivo, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Variable(models.Model):
    nombre = models.CharField(max_length=25)
    etiqueta = models.CharField(
        max_length=50, unique=True)  # Ver si quitar o no
    descripcion = models.TextField(max_length=200)
    unidad = models.CharField(max_length=25)
    valor_min_permitido = models.DecimalField(
        max_digits=12, decimal_places=3, null=True, blank=True,)
    valor_max_permitido = models.DecimalField(
        max_digits=12, decimal_places=3, null=True, blank=True,)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    dispositivo = models.ForeignKey(
        Dispositivo, related_name='variables', on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.nombre

    def __str__(self):
        return "%s - %s" % (self.id, self.nombre)


class Alerta(models.Model):
    variable = models.OneToOneField(
        Variable, on_delete=models.CASCADE, primary_key=True)
    min_alerta = models.DecimalField(max_digits=12, decimal_places=3)
    max_alerta = models.DecimalField(max_digits=12, decimal_places=3)
    phone = PhoneNumberField(null=False, blank=False, unique=True)
    descripcion = models.TextField(max_length=200, blank=False)
    ultimo_envio = models.DateTimeField(blank=False, null=True)

    def __str__(self):
        return self.descripcion

# https://stackoverflow.com/questions/14666199/how-do-i-create-multiple-model-instances-with-django-rest-framework/40862505#40862505


class Dato(models.Model):
    valor = models.DecimalField(max_digits=12, decimal_places=3)
    fecha = models.DateTimeField(null=False, default=now)
    variable = models.ForeignKey(
        Variable, related_name='datos', on_delete=models.CASCADE)
    # dispositivo = models.ForeignKey(
    #     Dispositivo, related_name='datos', null=True, on_delete=models.CASCADE)

    def __str__(self):
        # return "Disp(%s) - Var(%s) - Val(%s) " % (self.dispositivo.id, self.variable.id, self.valor)
        return "Id(%s) - Var(%s) - Val(%s) " % (self.id, self.variable.id, self.valor)
