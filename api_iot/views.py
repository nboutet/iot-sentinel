
from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Dispositivo, Variable, Dato
from .serializers import DispositivoSerializer, VariableSerializer, DatoSerializer, DispositivoVariableSerializer
from .ApiAccessPolicy import ApiAccessPolicy
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination


class DispositivoViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    permission_classes = (ApiAccessPolicy, )
    serializer_class = DispositivoSerializer
    queryset = Dispositivo.objects.all()
    filter_backends = [DjangoFilterBackend]

    # Helper property here to make get_queryset logic
    # more explicit
    @property
    def access_policy(self):
        return self.permission_classes[0]

    # Ensure that current user can only see the models
    # they are allowed to see
    # def get_queryset(self):
    #     return self.access_policy.scope_queryset(
    #         self.request, Dispositivo.objects.all()
    #     )


class VariableViewSet(viewsets.ModelViewSet):
    permission_classes = (ApiAccessPolicy, )
    serializer_class = DispositivoVariableSerializer
    queryset = Variable.objects.all()
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self):
        return Variable.objects.filter(dispositivo=self.kwargs['dispositivo_pk'])

    @property
    def access_policy(self):
        return self.permission_classes[0]


class DatoViewSet(viewsets.ModelViewSet):
    permission_classes = (ApiAccessPolicy, )
    queryset = Dato.objects.all()
    # serializer_class = DatoSerializer(many=True)
    # model = Dato
    serializer_class = DatoSerializer

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['variable']

    # def get_queryset(self):
    #     return Variable.objects.filter(dispositivo_pk=self.kwargs['dispositivo_pk'], variable=self.kwargs['variable_pk'])

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @property
    def access_policy(self):
        return self.permission_classes[0]

    # def create(self, request, *args, **kwargs):
    #     data = request.data.get(
    #         "items") if 'items' in request.data else request.data
    #     many = isinstance(data, list)

    #     serializer = self.get_serializer(data=data, many=many)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


# -----------------------------------------------------------------------
"""


    Si es necesario customizar las vistas, utilizar el mecanismo siguiente  


 """
# from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView
# class DispositivoListView(ListAPIView):
#     queryset = Dispositivo.objects.all()
#     serializer_class = DispositivoSerializer


# class DispositivoDetailView(RetrieveAPIView):
#     queryset = Dispositivo.objects.all()
#     serializer_class = DispositivoSerializer


# class DispositivoCreateView(CreateAPIView):
#     queryset = Dispositivo.objects.all()
#     serializer_class = DispositivoSerializer


# class DispositivoUpdateView(UpdateAPIView):
#     queryset = Dispositivo.objects.all()
#     serializer_class = DispositivoSerializer


# class DispositivoDeleteView(DestroyAPIView):
#     queryset = Dispositivo.objects.all()
#     serializer_class = DispositivoSerializer
