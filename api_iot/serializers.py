from rest_framework import serializers
from .models import Dispositivo, Variable, Dato
from rest_framework_nested.relations import NestedHyperlinkedRelatedField, NestedHyperlinkedIdentityField
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer
from collections import OrderedDict

"""
    Un Serializer es XX cosa.

    Se utilizaron 3 clases de Serializers

    1- `ModelSerializer`: es un` Serializer` normal, excepto que:

        * Un conjunto de campos predeterminados se rellena automáticamente.
        * Se completa automáticamente un conjunto de validadores predeterminados.
        * Se proporcionan implementaciones predeterminadas `.create ()` y `.update ()`.

        El proceso de determinar automáticamente un conjunto de campos de serializador
        basado en los campos del modelo es razonablemente complejo, pero prácticamente
        no es necesario profundizar en la implementación.

        Si la clase `ModelSerializer` * no * genera el conjunto de campos que
        necesita declarar explícitamente los campos adicionales / diferentes en
        la clase serializador, o simplemente use una clase `Serializer`.

    2- 'HiperLinkedModelSerializer': Es un tipo de `ModelSerializer` que define relaciones de
        los modelos a través de hiperlinks en vez de relaciones a través de primary keys.
        Específicamente:

        * Se incluye un campo 'url' en lugar del campo 'id'.
        * Las relaciones con otras instancias son hipervínculos, en lugar de claves primarias.

    3-  'NestedHyperlinkedModelSerializer': Es un tipo de `ModelSerializer` que utiliza relaciones de modelos
        hipervinculadas con claves COMPUESTAS (Anidadas) en  lugar de relaciones a través de primary keys.
        Específicamente:

        * Se incluye un campo 'url' en lugar del campo 'id'.
        * Las relaciones con otras instancias son hipervínculos, en lugar de claves primarias.

"""


# class DatoListSerializer(serializers.ListSerializer):
#     dispositivo = serializers.PrimaryKeyRelatedField(
#         many=False, read_only=True)
#     variable = serializers.PrimaryKeyRelatedField(
#         many=False, read_only=True)

#     class Meta:
#         model = Dato
#         fields = ('valor', 'fecha', 'dispositivo', 'variable')

#     # def __init__(self, *args, **kwargs):
#     #     many = kwargs.pop('many', True)
#     #     super(DatoListSerializer, self).__init__(
#     #         many=many, *args, **kwargs)


class DatoSerializer(serializers.ModelSerializer):

    # dispositivo = serializers.PrimaryKeyRelatedField(
    #     many=False, read_only=False, queryset=Dispositivo.objects.all())

    # variable = serializers.PrimaryKeyRelatedField(
    #     many=False, read_only=False, queryset=Variable.objects.select_related(id=dispositivo))

    class Meta:
        model = Dato
        # fields = ('id', 'valor', 'fecha', 'dispositivo', 'variable')
        fields = ('id', 'valor', 'fecha', 'variable')

    """
    Validations on specific fields
    """

    def validate(self, data):

        if(not isinstance(data, OrderedDict)):
            raise serializers.ValidationError(
                "Formato inválido. Formato correcto: List de Datos: [{" + "valor: x, variable: z" + "},]")
            return False

        data = dict(data)
        # dispositivo_id = data['dispositivo'].id
        # variable_dispostivo_id = data['variable'].dispositivo.id
        # if(dispositivo_id != variable_dispostivo_id):
        #     raise serializers.ValidationError(
        #         "La variable enviada no esta definida para el dispositivo")

        minVal = data['variable'].valor_min_permitido
        maxVal = data['variable'].valor_max_permitido

        if minVal is not None and data['valor'] < minVal:
            raise serializers.ValidationError(
                "Valor recibido inferior al mínimo permitido")
        elif maxVal is not None and data['valor'] > maxVal:
            raise serializers.ValidationError(
                "Valor recibido superior al máximo permitido")

        return data


# class VariableSerializer(serializers.HyperlinkedModelSerializer):
class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = ('id', 'nombre', 'descripcion', 'etiqueta', 'unidad',
                  'valor_min_permitido', 'valor_max_permitido', 'fecha_creacion', 'dispositivo', 'url')

        url = serializers.HyperlinkedIdentityField(
            read_only=True,
            view_name='variables-detail',
            # parent_lookup_kwargs={'pk': 'pk'}
        )


"""
Serializer del modelo 'Variable'.
Además de los campos del propio modelo, se define:
  * Una URL de cada Variable


URLs:
    List: < http://127.0.0.1:8000/api/dispositivos/1/variables/>
    Detail, Update, Get, Delete: < http://127.0.0.1:8000/api/dispositivos/1/variables/1/ >

Estructura:

"""


# class DispositivoVariableSerializer(serializers.HyperlinkedModelSerializer):
class DispositivoVariableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Variable
        fields = ('id', 'nombre', 'descripcion', 'etiqueta', 'unidad',
                  'valor_min_permitido', 'valor_max_permitido', 'fecha_creacion', 'dispositivo', 'url')

    url = NestedHyperlinkedIdentityField(
        read_only=True,
        view_name='dispositivo-variables-detail',
        parent_lookup_kwargs={'dispositivo_pk': 'dispositivo__pk'}
    )

    # dispositivoUrl = NestedHyperlinkedRelatedField(
    #     read_only=True,
    #     view_name='dispositivo',
    #     parent_lookup_kwargs={'dispositivo_pk': 'dispositivo__pk'}
    # )


"""
Serializer basado en el modelo 'Dispositivo'.
Además de los campos del propio modelo, se define:
  * Una URL de cada dispositivo específico
  * Por cada dispositivo, un arreglo de variables "anidadas" a través de 'DispositivoVariableSerializer'

URLs:
    List: < http://127.0.0.1:8000/api/dispositivos/ >
    Detail, Update, Get, Delete: < http://127.0.0.1:8000/api/dispositivos/1/ >

Estructura:
    [{
        "id": 1,
        "etiqueta": "EstacionX",
        ...,
        "url": "http://127.0.0.1:8000/api/dispositivos/1/",
        "variables": [
            {
                "id": 1,
                "nombre": "Temperatura",
                ...,
                "url": "http://127.0.0.1:8000/api/dispositivos/1/variables/1/"
            }
        ]
    }]
"""


# class DispositivoSerializer(serializers.HyperlinkedModelSerializer):
class DispositivoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dispositivo
        fields = ('id', 'etiqueta', 'descripcion', 'estado',
                  'fecha_creacion', 'ultima_actividad', 'latitud', 'longitud', 'url', 'variables')

    variables = DispositivoVariableSerializer(
        many=True,
        read_only=True)

    # url = serializers.HyperlinkedIdentityField(
    #     read_only=True,
    #     view_name='dispositivo-detail',
    # )

    url = NestedHyperlinkedIdentityField(
        read_only=True,
        view_name='dispositivo-detail',
        parent_lookup_kwargs={'pk': 'pk'}
    )

    # variables = NestedHyperlinkedRelatedField(
    #     many=True,
    #     read_only=True,   # Or add a queryset
    #     view_name='dispositivo-variables-detail',
    #     parent_lookup_kwargs={'dispositivo_pk': 'dispositivo_pk'}
    # )
