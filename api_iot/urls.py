
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework_nested import routers
from .views import DispositivoViewSet, VariableViewSet, DatoViewSet


# Nested Routing
router = routers.SimpleRouter()
router.register(r'dispositivos', DispositivoViewSet)

dispositivos_router = routers.NestedSimpleRouter(
    router, r'dispositivos', lookup='dispositivo')
dispositivos_router.register(
    r'variables', VariableViewSet, base_name='dispositivo-variables')


router.register('datos', DatoViewSet, base_name='dato')

# routerx = routers.DefaultRouter()
# urlpatterns = router.urls


urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'', include(dispositivos_router.urls)),
    # re_path(r'', include(router.urls)),
    # re_path(r'', include())
    # re_path(r'', include((dispositivos_router.urls, 'api_iot'),
    #                      namespace='dispositivo-variables')),
]


# dispositivo_list = DispositivoViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })
# dispositivo_detail = DispositivoViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })

# variable_list = VariableViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })

# variable_detail = VariableViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })

# urlpatterns = [
#     re_path(r'', include(router.urls)),
#     re_path(r'', include(dispositivos_router.urls)),
#     # re_path(r'', include((dispositivos_router.urls, 'api_iot'),
#     #                      namespace='dispositivo-variables')),


#     # path('dispositivos/<int:pk>/', dispositivo_detail, name='dispositivo-detail'),
#     # path('dispositivos/<int:pk>/', dispositivo_list, name='dispositivo-list'),
#     # path('dispositivos/<int:pk_dev>/variables/<int:pk_var>/',
#     #      variable_detail, name='dispositivo-variables-detail'),
# ]


# urlpatterns = [
#     path('dispositivos/', include(router.urls)),
#     path('schemas/', schema_view),
# ]


""" from .views import (
    DispositivoListView,
    DispositivoDetailView,
    DispositivoCreateView,
    DispositivoUpdateView,
    DispositivoDeleteView
)


urlpatterns = [
    path('', DispositivoListView.as_view()),
    path('create/', DispositivoCreateView.as_view()),
    path('<pk>', DispositivoDetailView.as_view()) ,
    path('<pk>/delete/', DispositivoDeleteView.as_view()),
    path('<pk>/update/', DispositivoUpdateView.as_view())
] """
