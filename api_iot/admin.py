from django.contrib import admin

# Register your models here.
from .models import Dispositivo, Variable, Alerta, Propiedad, Dato

admin.site.register(Dispositivo)
# admin.site.register(Localizacion)
admin.site.register(Propiedad)
admin.site.register(Variable)
admin.site.register(Alerta)
admin.site.register(Dato)
