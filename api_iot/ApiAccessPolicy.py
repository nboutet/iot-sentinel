from rest_access_policy import AccessPolicy
from pprint import pprint


class ApiAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": "*",
            "effect": "allow",
            "condition": "is_staff"
        },
        # {
        #     "action": ["list", "retrieve", "options"],
        #     "principal": "*",
        #     "effect": "allow",
        #     "condition": "is_not_staff"
        # },
        # {
        #     "action": ["list", "retrieve", "options"],
        #     "principal": "*",
        #     "effect": "allow",
        #     "condition": "*"
        # },



        # {
        #     "action": ["list", "retrieve"],
        #     "principal": "*",
        #     "effect": "allow"
        # },
        # {
        #     "action": ["publish", "unpublish"],
        #     "principal": ["group:editor"],
        #     "effect": "allow"
        # },
        # {
        #     "action": ["delete"],
        #     "principal": ["*"],
        #     "effect": "allow",
        #     "condition": "is_author"
        # },
        # {
        #     "action": ["*"],
        #     "principal": ["*"],
        #     "effect": "deny",
        #     "condition": "is_happy_hour"
        # }
    ]

    def is_staff(self, request, view, action) -> bool:
        return request.user.is_staff

    def is_not_staff(self, request, view, action) -> bool:
        return not request.user.is_staff

    # @classmethod
    # def scope_queryset(cls, request, queryset):
    #     if request.user.groups.filter(name='editor').exists():
    #         return queryset

    #     return queryset.filter(status='published')
